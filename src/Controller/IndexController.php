<?php

namespace App\Controller;


use App\Entity\User;
use App\Model\Api\ApiContext;

use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class IndexController extends Controller
{

    /**
     * @Route("/", name="homePage")
     * @param ApiContext $apiContext
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(ApiContext $apiContext, Request $request)
    {
        $form = $this->createForm('App\Form\SearchFeeldType');
        $form->handleRequest($request);
        $dataSearch =[];
        if ($form->isSubmitted()) {
            $formData = $form->getData();

            $data = $apiContext->search($formData['word'], $formData['sort'], $formData['limit']);

            $datas = $data['data']['children'];

            foreach ($datas as $key => $val) {

                $dataReddit = ['id' => '', 'thumbnail' => '', 'title' => '', 'author' => '', 'created_utc' => ''];

                foreach ($val['data'] as $key2 => $attr) {

                    if ($key2 === 'title') {
                        $dataReddit['title'] = $attr;

                    }
                    if ($key2 === 'thumbnail') {
                        $dataReddit['thumbnail'] = $attr;

                    }
                    if ($key2 === 'author') {
                        $dataReddit['author'] = $attr;

                    }
                    if ($key2 === 'created_utc') {
                        $date = date("d-m-Y",$attr);
                        $dataReddit['created_utc'] = $date ;
                    }
                    if($key2 === 'name'){
                        $dataReddit['name'] = $attr;
                    }

                }

                if (!empty($dataReddit['title']) || !empty(($key2 === 'thumbnail')
                        || !empty(($key2 === 'authot') || !empty(($key2 === 'created_utc'))))) {

                    $dataSearch[] = $dataReddit;
                }

            }

                return $this->render('index.html.twig', [
                    'form' => $form->createView(),
                    'dataSearch' => $dataSearch
                ]);

        }
        return $this->render('index.html.twig', [
            'form' => $form->createView(),
            'dataSearch' => $dataSearch
        ]);
    }

    /**
     * @Route("/registration", name="app_registration")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @param UserRepository $repository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registrationAction(Request $request, ObjectManager $manager ,UserHandler $userHandler, UserRepository $repository)
    {
        $form = $this->createForm('App\Form\RegistrationType');
        $form->handleRequest($request);
        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if($repository->findOneByEmail($data['email'])){
                $error = 'Такой email уже существует введите другой email';
                $this->redirectToRoute('app_registration');
            }else{

                $user = $userHandler->createNewUser($data);
                $manager->persist($user);
                $manager->flush();
                $userHandler->makeUserSession($user);

                return $this->redirectToRoute('homePage');
            }
        }
        return $this->render('Registr.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);

    }

    /**
     * @Route("/authorization", name="app_authorization")
     * @param Request $request
     * @param UserRepository $repository
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function authorizationAction(Request $request, UserRepository $repository, UserHandler $userHandler)
    {
        $form = $this->createForm('App\Form\AuthType');
        $form->handleRequest($request);
        $error = null;
        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $user = $repository->findOneByPasswordAndEmail($data['email'],$data['password']);

            if($repository->findOneByPasswordAndEmail($data['email'],$data['password'])){
                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('homePage');
            }
                $error = 'Такого пользователя нет в базе данных или не вернй паролль';
            return $this->render('auth.html.twig', [
                'form' => $form->createView(),
                'error' => $error
            ]);

        }
        return $this->render('auth.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);

    }

    /**
     * @Route("/displayPage/{name}", name="app_display")
     * @param $name
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function goToPage($name, ApiContext $apiContext)
    {
        $data =  $apiContext->searchDetails($name);
        $detailData = $data['data']['children'][0];

        return $this->render('details.html.twig',[
            'detailData' => $detailData
        ]);
    }


}