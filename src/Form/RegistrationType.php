<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,['label'=> 'Введиет ваше имя'])
            ->add('surname',TextType::class,['label'=> 'Введиет вашу фамилию'])
            ->add('email', EmailType::class,['label'=> 'Введиет ваш e-mail'])
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class)
        ;
    }
}
