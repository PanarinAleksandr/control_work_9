<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

;

class SearchFeeldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('word', TextType::class, [
                'label' => 'Поиск'
            ])
            ->add('sort', ChoiceType::class, [

                'choices' => [
                    'По релевантности' => 'relevance',
                    'Горячеие' => 'hot',
                    'В топе' => 'top',
                    'Новые' => 'new',
                    'Комментируемые' => 'comments'
                ]
            ])
            ->add('limit', ChoiceType::class, [
                'placeholder' => 'Сколько?',
                'choices' => [
                    '8 шт' => 8,
                    '12 шт' => 12,
                    '16 шт' => 16,
                    '50 шт' => 50,
                    '100 шт' => 100
                ]])
            ->add('save', SubmitType::class, ['label' => 'Искать']);
    }
}
