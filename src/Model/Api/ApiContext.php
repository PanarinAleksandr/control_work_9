<?php

namespace App\Model\Api;


class ApiContext extends AbstractApiContext
{
    const ENDPOINT_SEARCH = '/search.json';
    const ENDPOINT_SEARCH_DETAIL = '/api/info.json';

    public function search($searchWord, $sort, $limit)
    {
        return $this->makeQuery(self::ENDPOINT_SEARCH, self::METHOD_GET,
            ['q' => $searchWord,'sort' => $sort,'limit' => $limit ,'type'=>'link']);
    }

    public function searchDetails($id)
    {
        return $this->makeQuery(self::ENDPOINT_SEARCH_DETAIL, self::METHOD_GET,
            ['id'=> $id]);
    }

}
